/* mbed Stepper Library
 * Copyright (c) 2010 fachatz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "Stepper.h"
#include "mbed.h"

// define Library version number
#define VERSION 0.3

// led4, used for testing the direction signal
DigitalOut led4(LED4);

Stepper::Stepper(PinName clk, PinName dir, uint16_t _startStopSpeed) : _clk(clk), _dir(dir) {
	_clk = 0, _dir = 0;
	startStopSpeed = _startStopSpeed;
}

void Stepper::step(int n_steps, bool direction, int speed, bool accel) {
	int accelspeed;
	if (accel) {
		accelspeed = startStopSpeed;
	} else {
		accelspeed = speed;
	}

	for (int i=0; i<n_steps; i++) {

		// linear acceleration
		if (accel) {
			if(i < startStopSpeed && --accelspeed == speed) accelspeed ++;
			if(i > (n_steps - startStopSpeed) && ++accelspeed == startStopSpeed) accelspeed--;
		}
		_dir = led4 = direction;
		_clk = 1;
		wait_us(1);
		_clk = 0;
		wait_us(1);
		wait_us(accelspeed);
	}
}

// version() returns the version number of the library
float Stepper::version(void) {
	return VERSION;
}
